<?php
session_start();
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Confirmation</title>
</head>

<body>
    <main>
        <form action="confirmation.php" method="post">
            <div class="container">
                <?php
                $name = $_SESSION["name"];
                $gender = $_SESSION["gender"];
                $faculty = $_SESSION["faculty"];
                $birth = $_SESSION["birth"];
                $address = $_SESSION["address"];
                $image = $_SESSION["image"];
                ?>
                <div>
                    <label for="name">Họ và tên</label>
                    <?php
                    echo $name . '</br>';
                    ?>
                </div>

                <div>
                    <label for="gender">Giới tính</label>
                    <?php
                    echo $gender . '</br>';
                    ?>
                </div>

                <div>
                    <label for="faculty">Phân khoa</label>
                    <?php
                    echo $faculty . '</br>';
                    ?>
                </div>

                <div>
                    <label for="birth">Ngày sinh</label>
                    <?php
                    echo $birth . '</br>';
                    ?>
                </div>

                <div>
                    <label for="address">Địa chỉ</label>
                    <?php
                    echo $address . '</br>';
                    ?>
                </div>

                <div>
                    <label for="address">Hình ảnh</label>
                    <?php
                    if (file_exists($image)) {
                        echo "<img src=$image width='200px'>";
                    }
                    ?>
                </div>

                <button type="submit" class="button" name="register">Xác nhận</button>
        </form>
    </main>
</body>

</html>