<?php
session_start();

$genders = array(
    0 => "Nam",
    1 => "Nữ",
);

$faculties = array(
    "None" => "",
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu",
);

$keyword_faculty = $keyword_name = "";
$_SESSION["keyword_faculty"] = $_SESSION["keyword_name"]  = "";

$fake_data = array(
    array("name" => "Linhh", "faculty" => "Khoa học máy tính"),
    array("name" => "Hààà ", "faculty" => "Khoa học máy tính"),
    array("name" => "Phươnggg", "faculty" => "Khoa học vật liệu"),
    array("name" => "Tranggg", "faculty" => "Khoa học vật liệu"),
);
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style_update.css">
    <title>Update</title>
</head>

<body>
    <main>
        <form action="update.php" method="post">
            <div class="container">
                <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST") {

                    if (!is_null($_POST["keyword_faculty"])) {
                        $_SESSION["keyword_faculty"] = $faculties[$_POST["keyword_faculty"]];
                    }
                    if (!empty($_POST["keyword_name"])) {
                        $_SESSION["keyword_name"] = $_POST["keyword_name"];
                    }
                }
                ?>
                <div>
                    <label for="keyword_faculty">Khoa</label>
                    <select name="keyword_faculty">
                        <?php
                        foreach ($faculties as $key => $value) {
                            if ($faculties[$key] == $_SESSION["keyword_faculty"]) {
                                $selected = "selected";
                            } else {
                                $selected = "";
                            }
                            echo "<option value=$key $selected>$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div>
                    <label for="keyword_name">Từ khóa</label>
                    <input type="text" class="input" name="keyword_name" value='<?php echo ($_SESSION["keyword_name"]); ?>'>
                </div>

                <button type="submit" class="button" name="search">Tìm kiếm</button>

                <div>
                    Số sinh viên tìm thấy: <?php echo count($fake_data); ?>
                </div>

                <div>
                    <button type="button" onclick="location.href = 'register.php';" class="button" name="register" style="margin: 8px 725px;">Thêm</button>
                </div>

                <table>
                    <tr>
                        <th>STT</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Hành động</th>
                    </tr>

                    <?php
                    $count = 1;
                    foreach ($fake_data as $data) {
                        echo "<tr>
                        <td style='width: 100px;'>" . $count .
                            "<td style='width: 600px;'>" . $data["name"] .
                            "</td><td style='width: 400px;'>" . $data["faculty"] .
                            "</td><td style='width: 400px;'>" . "<button type='button' style='margin: 8px 8px; border-radius: 0px; text-align: center; width: auto;'>Sửa</button>" . "<button type='button' style='margin: 8px 8px; border-radius: 0px; text-align: center; width: auto;''>Xóa</button>" .
                            "</td>
                        </tr>";
                        $count++;
                    }
                    ?>
                </table>
        </form>
    </main>
</body>

</html>